import os
import fnmatch
import glob
import xlrd
import xlsxwriter

# Example path: F:\Work Projects\Aidin\Python_test\Python_test

# getting the directory path
from xlsxwriter import Workbook

path = input('Please enter the parent directory\'s path: ')

target_files = []
angels = []
frequencies = []
data = []
deleted_data = []

# fetching all folders
folders = os.listdir(path)
for folder in folders:
    # fetching all files in any folder
    files = os.listdir(path + "\\" + folder)
    for target in glob.glob(path + "/" + folder + "/*main*.xlsx"):
        # add the filtered file to list
        target_files.append(target)
    for target in glob.glob(path + "/" + folder + "/*main*.xlsm"):
        # add the filtered file to list
        target_files.append(target)

# opening excel files
for file, target_file in enumerate(target_files):
    wb = xlrd.open_workbook(target_file)
    sheet = wb.sheet_by_index(0)

    # fetch angels
    for angel in range(sheet.ncols):
        if angel > 0:
            angel_dict = {
                'fi': file,
                'v': sheet.cell_value(1, angel)
            }
            angels.append(angel_dict)

    # fetch frequencies
    for frequency in range(sheet.nrows):
        if frequency > 1:
            frequency_dict = {
                'fi': file,
                'v': sheet.cell_value(frequency, 0)
            }
            frequencies.append(frequency_dict)

    row_counter = 1
    col_counter = 1
    # fetching data
    for r in range(sheet.nrows):
        for c in range(sheet.ncols):
            if r and c > 0:
                data_dict = {
                    'fi': file,
                    'f': sheet.cell_value(r, 0),
                    'a': sheet.cell_value(1, c),
                    'v': sheet.cell_value(r, c)
                }
                if data_dict['f'] != 'A':
                    data.append(data_dict)

# sort elements
sorted_angels = sorted(angels, key=lambda a: a['v'])
sorted_frequencies = sorted(frequencies, key=lambda f: f['v'])
sorted_data = sorted(data, key=lambda d: d['f'])

# delete duplicated angles
for an_i, an in enumerate(sorted_angels):
    for an_i_c, ch in enumerate(sorted_angels):
        if an_i_c != 0:
            if sorted_angels[an_i]['v'] == sorted_angels[an_i_c]['v']:
                sorted_angels.remove(ch)

# delete duplicated frequencies
for fr_i, fr in enumerate(sorted_frequencies):
    for fr_i_p, fr_p in enumerate(sorted_frequencies):
        if fr_i_p != 0:
            if sorted_frequencies[fr_i]['v'] == sorted_frequencies[fr_i_p]['v']:
                sorted_frequencies.remove(fr_p)

# delete duplicated cells and choose min of data for cell
for cell_i, cell in enumerate(sorted_data):
    for cell_i_p, cell_pointer in enumerate(sorted_data):
        if sorted_data[cell_i]['f'] == sorted_data[cell_i_p]['f'] and sorted_data[cell_i]['a'] == sorted_data[cell_i_p]['a']:
            if cell_i != cell_i_p:
                if sorted_data[cell_i]['v'] <= sorted_data[cell_i]['v']:
                    deleted_data.append(sorted_data[cell_i])
                    sorted_data.remove(sorted_data[cell_i])
                elif sorted_data[cell_i]['v'] >= sorted_data[cell_i]['v']:
                    deleted_data.append(sorted_data[cell_i_p])
                    sorted_data.remove(sorted_data[cell_i_p])


workbook: Workbook = xlsxwriter.Workbook(path + '\\Result.xlsx')
worksheet = workbook.add_worksheet()

# Add a bold format to use to highlight cells.
bold = workbook.add_format({'bold': True})

# write frequencies
for s_f_i, s_f in enumerate(sorted_frequencies):
    worksheet.write(s_f_i + 2, 0, s_f['v'], bold)

# write angles
for s_a_i, s_a in enumerate(sorted_angels):
    worksheet.write(1, s_a_i + 1, s_a['v'], bold)

for a_p in range(len(sorted_angels) + 1):
    if a_p == 0:
        worksheet.write(0, 0, '')
    else:
        worksheet.write(0, a_p, 'B')
worksheet.write(1, 0, 'A')

# writing data
for d_f_p, d_f in enumerate(sorted_frequencies):
    for d_a_p, d_a in enumerate(sorted_angels):
        for item in sorted_data:
            if item['f'] == d_f['v'] and item['a'] == d_a['v']:
                format_ = None
                cell_format = workbook.add_format()
                if item['v'] != '':
                    if item['v'] >= 0:
                        cell_format.set_pattern(1)
                        cell_format.set_bg_color('green')
                    else:
                        cell_format.set_pattern(1)
                        cell_format.set_bg_color('red')
                    worksheet.write(d_f_p + 2, d_a_p + 1, item['v'], cell_format)
                else:
                    worksheet.write(d_f_p + 2, d_a_p + 1, item['v'])

# result logs
print(f'+--------------------------------------------------------------+')
print(f'| Cuddly finished the process!                            Logs |')
print(f'+--------------------------------------------------------------+')
print('')
print(f'+--------------------------------------------------------------+')
print(f'| Sorted data                                                  |')
print(f'+--------------------------------------------------------------+')
for a, i in enumerate(sorted_data):
    print(f'index[{a}] ---> {i}')

print(f'+--------------------------------------------------------------+')
print(f'| Deleted data                                                 |')
print(f'+--------------------------------------------------------------+')
for a, i in enumerate(deleted_data):
    print(f'index[{a}] ---> {i}')

workbook.close()
